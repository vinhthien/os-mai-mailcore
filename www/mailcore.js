var MailCore = function() {};

/**
 * Verify email and password
 * @param {string} username - The username of mail user. (e.g: your_email@gmail.com)
 * @param {string} password - The password of mail user.
 */
MailCore.prototype.authenticate = function(hostname, port, username, password, connectionType, success, fail) {
  cordova.exec(success, fail, "MailCorePlugin", "authenticate", [hostname, port, username, password, connectionType]);
};

MailCore.prototype.fetchAttachment = function(hostname, port, username, password, connectionType, messageUID, partID, partEncoding, filename, success, fail) {
  cordova.exec(success, fail, "MailCorePlugin", "fetchAttachment", [hostname, port, username, password, connectionType, messageUID, partID, partEncoding, filename]);
};

MailCore.prototype.syncMessages = function(hostname, port, username, password, connectionType, lastMessageKnownUID, hablaxServerEmail, hablaxEmailsAccepted, success, fail) {
  cordova.exec(success, fail, "MailCorePlugin", "syncMessages", [hostname, port, username, password, connectionType, lastMessageKnownUID, hablaxServerEmail, hablaxEmailsAccepted]);
};

/**
* Imap fetch all messages
* @param {string} hostname - The hostname of mail server. (e.g: imap.gmail.com)
* @param {string} port - The Imap port of mail server. (e.g: 993)
* @param {string} username - The username of mail user. (e.g: your_email@gmail.com)
* @param {string} password - The password of mail user.
* @param {string} connectionType - The connection type. (options: "TLS", "StartTLS", "Clear")
*/
MailCore.prototype.imapFetchAllMessages = function(hostname, port, username, password, connectionType, success, fail) {
  cordova.exec(success, fail, "MailCorePlugin", "imapFetchAllMessages", [hostname, port, username, password, connectionType]);
};

/**
 * Smtp send a message
 * @param {string} hostname - The hostname of mail server. (e.g: smtp.gmail.com)
 * @param {string} port - The Imap port of mail server. (e.g: 465)
 * @param {string} username - The username of mail user. (e.g: your_email@gmail.com)
 * @param {string} password - The password of mail user.
 * @param {string} connectionType - The connection type. (options: "TLS", "StartTLS", "Clear")
 * @param {string} recipient - The recipient will be received. (e.g: your_recipient@gmail.com)
 * @param {string} subject - The subject of email.
 * @param {string} body - The body of email.
 */
MailCore.prototype.smtpSendMessage = function(hostname, port, username, password, connectionType, recipient, subject, body, attachmentPath, success, fail) {
  cordova.exec(success, fail, "MailCorePlugin", "smtpSendMessage", [hostname, port, username, password, connectionType, recipient, subject, body, attachmentPath]);
};

MailCore.prototype.listenNewMessages = function(hostname, port, username, password, connectionType, lastKnownMessageUID, hablaxServerEmail, hablaxEmailsAccepted, success, fail) {
  cordova.exec(success, fail, "MailCorePlugin", "listenNewMessages", [hostname, port, username, password, connectionType, lastKnownMessageUID, hablaxServerEmail, hablaxEmailsAccepted]);
};

MailCore.prototype.killListenerOperations = function(success, fail) {
  cordova.exec(success, fail, "MailCorePlugin", "killListenerOperations", []);
};

MailCore.prototype.updateHablaxServerEmail = function(hostname, port, username, password, connectionType, hablaxServerEmail, success, fail) {
  cordova.exec(success, fail, "MailCorePlugin", "updateHablaxServerEmail", [hostname, port, username, password, connectionType, hablaxServerEmail]);
};

MailCore.prototype.updateHablaxEmailsAccepted = function(hostname, port, username, password, connectionType, hablaxEmailsAccepted, success, fail) {
  cordova.exec(success, fail, "MailCorePlugin", "updateHablaxEmailsAccepted", [hostname, port, username, password, connectionType, hablaxEmailsAccepted]);
};

var mailCore = new MailCore();
module.exports = mailCore;

